package com.wide.repository;

import java.util.List;

import com.wide.domain.Order;
import com.wide.domain.OrderItem;
import com.wide.domain.Product;

public interface OrderRepository {
	void save (Order order) throws RepositoryException;
	List<Order> findAll() throws RepositoryException;
	Order findById(int idOrder) throws RepositoryException;
	List<Product> productList() throws RepositoryException;
	void addProduct(Product product) throws RepositoryException;
	void updateProduct(int idProduct, Product product) throws RepositoryException;
	void deleteProduct(int idProduct) throws RepositoryException;
	List<Product> addProductOrderItem(Product product, int quantity) throws RepositoryException;
}
