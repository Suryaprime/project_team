package com.wide.repository.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.wide.domain.Order;
import com.wide.domain.OrderItem;
import com.wide.domain.Product;
import com.wide.repository.OrderRepository;
import com.wide.repository.RepositoryException;

public class OrderRepositoryMySQL implements OrderRepository {
	public void save(Order order) throws RepositoryException {
		Connection conn = null;
		PreparedStatement orderItemStm = null;
		PreparedStatement orderIdStm = null;
		PreparedStatement orderStm = null;
		Set<Integer> setId = new HashSet<Integer>();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/project_team", "root", "");
			conn.setAutoCommit(false);
			
			String sqlOrderItem =  "INSERT INTO order_item_tbl(price, quantity, product_id_fk, order_id_fk) VALUES(?,?,?,?)";
			orderItemStm = conn.prepareStatement(sqlOrderItem);
			String sqlOrderId = "SELECT * FROM order_tbl ORDER BY order_id DESC LIMIT 1";
			orderIdStm = conn.prepareStatement(sqlOrderId);
			String orderSQL = "INSERT INTO order_tbl(order_id) VALUES(?)";
			orderStm = conn.prepareStatement(orderSQL);
			
			orderStm.setInt(1, order.getId());
            orderStm.executeUpdate();
			
            ResultSet rs = orderIdStm.executeQuery();
			while(rs.next()) {
				int orderId = rs.getInt("order_id");
				
				for(int i=0; i < order.getOrderItem().size(); i++) {
		            var param = order.getOrderItem().get(i);
		            orderItemStm.setDouble(1, param.getPrice());
		            orderItemStm.setInt(2, param.getQuantity());
		            orderItemStm.setInt(3, param.getProductIdFk());
		            orderItemStm.setInt(4, orderId);
		            orderItemStm.executeUpdate();
		            
		        }
			}
			conn.commit();
			orderItemStm.close();
			orderIdStm.close();
	        orderStm.close();
	        conn.close();
	    } catch (ClassNotFoundException e) {
	        throw new RepositoryException("masalah setup driver");
	    } catch (SQLException e) {
	        if(conn != null) {
	            try {
	                conn.rollback();
	            } catch (SQLException e1) {
	                throw new RepositoryException("roll back gagal");
	            }
	        }
	        e.printStackTrace();
	        throw new RepositoryException("proses sql gagal");
	    }

	}

	@Override
	public Order findById(int idOrder) throws RepositoryException {
		Order order = new Order(idOrder);
		
		String userDb = "root";
		String passwordDb = "";
		String jdbcUrl = "jdbc:mysql://localhost/project_team";
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			Connection conn = DriverManager.getConnection(jdbcUrl, userDb, passwordDb);
			String sql = "SELECT order_tbl.id,orderitem.order_id,orderitem.CODE,orderitem.NAME,orderitem.`type`,orderitem.price,orderitem.quantity\r\n"
					+ "FROM order_tbl\r\n"
					+ "INNER JOIN orderitem ON order_tbl.id = orderitem.order_id;";
			
			java.sql.Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sql);
			
			while (rs.next()) {
				if (idOrder == rs.getInt("order_tbl.id")) {
//					OrderItem item = new OrderItem(rs.getString("orderitem.name"), rs.getDouble("orderitem.price"), rs.getInt("orderitem.quantity"));
//					order.addItem(item);
				}
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
			return order;
		}

	@Override
	public List<Order> findAll() throws com.wide.repository.RepositoryException {
		List<Order> orders = new ArrayList<>();
		Set<Integer> ordersId = new HashSet<>();
		
		String userDb = "root";
		String passwordDb = "";
		String jdbcUrl = "jdbc:mysql://localhost/project_team";
		Connection conn = null;
		PreparedStatement findAllStm = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			conn = DriverManager.getConnection(jdbcUrl, userDb, passwordDb);
			conn.setAutoCommit(false);
			String queryFindAll = "SELECT * FROM order_item_tbl";
			findAllStm = conn.prepareStatement(queryFindAll);
			ResultSet rs = findAllStm.executeQuery();
			while(rs.next()) {
				String name = rs.getString("name");
				double price = rs.getDouble("price");
				int quantity = rs.getInt("quantity");
				int orderFK = rs.getInt("order_id_fk");
				int productFK = rs.getInt("product_id_fk");
				if(ordersId.contains(orderFK)) {
					for(int i=0; i < ordersId.size(); i++) {
						if(orders.get(i).getId() == orderFK) {
//							orders.get(i).addItem(new OrderItem(name, price, quantity));
						}
					}
				}else {
					ordersId.add(orderFK);
					Order order = new Order(orderFK);
//					order.addItem(new OrderItem(name, price, quantity));
					orders.add(order);
				}
			}
			//jgn lupa close
			conn.commit();
			findAllStm.close();
			conn.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return orders;
	}

	@Override
	public List<Product> productList() throws RepositoryException {
		List<Product> products = new ArrayList<Product>();

		String userDb = "root";
		String passwordDb = "";
		String jdbcUrl = "jdbc:mysql://localhost/project_team";
		Connection conn = null;
		PreparedStatement productListStm = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(jdbcUrl, userDb, passwordDb);
			conn.setAutoCommit(false);
			String queryListProduct = "SELECT * FROM product_tbl";
			productListStm = conn.prepareStatement(queryListProduct);
			ResultSet rs = productListStm.executeQuery();
			while(rs.next()) {
				int productId = rs.getInt("product_id");
				String code = rs.getString("code");
				String name = rs.getString("name");
				String type = rs.getString("type");
				double price = rs.getDouble("price");
				Product product = new Product(productId, code, name, type, price);
				products.add(product);
			}
			//jgn lupa close
			conn.commit();
			productListStm.close();
			conn.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return products;
	}

	@Override
	public void addProduct(Product product) throws RepositoryException {
		Set<String> productCode = new HashSet<String>();

		String userDb = "root";
		String passwordDb = "";
		String jdbcUrl = "jdbc:mysql://localhost/project_team";
		Connection conn = null;
		PreparedStatement productCodeListStm = null;
		PreparedStatement addProductStm = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(jdbcUrl, userDb, passwordDb);
			conn.setAutoCommit(false);
			String queryAddProduct = "INSERT INTO product_tbl(code, name, type, price) VALUES(?,?,?,?)";
			addProductStm = conn.prepareStatement(queryAddProduct);
            
            String queryListProduct = "SELECT * FROM product_tbl";
			productCodeListStm = conn.prepareStatement(queryListProduct);
			ResultSet rs = productCodeListStm.executeQuery();
			
			while(rs.next()) {
				String code = rs.getString("code");
				productCode.add(code);
			}
			if(productCode.contains(product.getCode()) == false) {
				addProductStm.setString(1, product.getCode());
				addProductStm.setString(2, product.getName());
				addProductStm.setString(3, product.getType());
				addProductStm.setDouble(4, product.getPrice());
				addProductStm.executeUpdate();
			}
			//jgn lupa close
			conn.commit();
			productCodeListStm.close();
			addProductStm.close();
			conn.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	public List<Product> addProductOrderItem(Product product, int quantity) throws RepositoryException {
		List<Product> products = new ArrayList<Product>();
		String userDb = "root";
		String passwordDb = "";
		String jdbcUrl = "jdbc:mysql://localhost/project_team";
		Connection conn = null;
		PreparedStatement listOrderItemStm = null;
		PreparedStatement addQuantityOrderItem = null;
		Set<Integer> productIdFkSet = new HashSet<Integer>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(jdbcUrl, userDb, passwordDb);
			conn.setAutoCommit(false);
			String queryListProduct = "SELECT * FROM order_item_tbl";
			listOrderItemStm = conn.prepareStatement(queryListProduct);
			String queryAddQuantityOrderItem = "UPDATE order_item_tbl SET quantity = (?) WHERE productIdFk = (?)";
			addQuantityOrderItem = conn.prepareStatement(queryAddQuantityOrderItem);
			ResultSet rs = listOrderItemStm.executeQuery();
			while(rs.next()) {
				int productIdFk = rs.getInt("product_id_fk");
				productIdFkSet.add(productIdFk);
			}
			if(productIdFkSet.contains(product.getProductId()) == false)  {
				products.add(product);
			} else {
				while(rs.next()) {
					int productIdFk = rs.getInt("product_id_fk");
					if(productIdFk == product.getProductId()) {
						int quantityOrderItem = rs.getInt("quantity");
						int totalQuantity = quantityOrderItem + quantity;
						addQuantityOrderItem.setInt(1, totalQuantity);
						addQuantityOrderItem.setInt(2, productIdFk);
						addQuantityOrderItem.executeUpdate();
					}
				}
			}
			conn.commit();
			listOrderItemStm.close();
			addQuantityOrderItem.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return products;
		
	}

	@Override
	public void updateProduct(int idProduct, Product product) throws RepositoryException {
		String userDb = "root";
		String passwordDb = "";
		String jdbcUrl = "jdbc:mysql://localhost:3306/project_team";
		Connection conn = null;
		PreparedStatement updateProductStm = null;
		PreparedStatement getProductListStm = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(jdbcUrl, userDb, passwordDb);
			conn.setAutoCommit(false);
			String queryUpdateProduct = "UPDATE product_tbl SET code=?,name=?,type=?,price=? WHERE product_id=?";
			updateProductStm = conn.prepareStatement(queryUpdateProduct);
			String queryGetProductList = "SELECT * FROM product_tbl";
			getProductListStm = conn.prepareStatement(queryGetProductList);
			ResultSet rs = getProductListStm.executeQuery();
			updateProductStm.setString(1, product.getCode());
			updateProductStm.setString(2, product.getName());
			updateProductStm.setString(3, product.getType());
			updateProductStm.setDouble(4, product.getPrice());
			updateProductStm.setInt(5, idProduct);
			while(rs.next()) {
					int productIdDatabase = rs.getInt("product_id");
					if(idProduct == productIdDatabase) {
						updateProductStm.executeUpdate();
				}
			}
			
			conn.commit();
			updateProductStm.close();
			getProductListStm.close();
			conn.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void deleteProduct(int idProduct) throws RepositoryException {
		String userDb = "root";
		String passwordDb = "";
		String jdbcUrl = "jdbc:mysql://localhost:3306/project_team";
		Connection conn = null;
		PreparedStatement deleteProductStm = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(jdbcUrl, userDb, passwordDb);
			conn.setAutoCommit(false);
			String queryDeleteProduct = "DELETE FROM product_tbl WHERE product_id = (?)";
			deleteProductStm = conn.prepareStatement(queryDeleteProduct);
			deleteProductStm.setInt(1, idProduct);
			deleteProductStm.executeUpdate();
			
			conn.commit();
			deleteProductStm.close();
			conn.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
