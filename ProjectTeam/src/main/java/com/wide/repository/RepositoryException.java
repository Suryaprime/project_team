package com.wide.repository;

public class RepositoryException extends Exception {
	public RepositoryException() {
		
	}
	
	public RepositoryException(String message) {
		super(message);
	}

}
