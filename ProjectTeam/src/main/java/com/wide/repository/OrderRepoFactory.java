package com.wide.repository;

import com.wide.repository.impl.OrderRepositoryFile;
import com.wide.repository.impl.OrderRepositoryMySQL;

public class OrderRepoFactory {
	private static String dbType = "MySQL";
	
	public static OrderRepository createRepository() {
		if ("MySQL".equals(dbType)) {
			return new OrderRepositoryMySQL();
		} else {
			return new OrderRepositoryFile();
		}
		
	}
}
