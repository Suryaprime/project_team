package com.wide.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Order {
	private int id;
	private Date createdAt;
	

	private List<OrderItem> orderItem = new ArrayList<OrderItem>();
	
	public Order(int id) {
		this.id = id;
	}

	public Order() {
		// TODO Auto-generated constructor stub
	}

	public double totalPrice() {
		double total = 0;
		for (int i=0; i < orderItem.size(); i++) {
			total += orderItem.get(i).totalPrice();
		}
		return total;
	}
	
	public void addItem(OrderItem orderItem) {
			this.orderItem.add(orderItem);
		}
	
	public List<OrderItem> getOrderItem() {
		return orderItem;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
}
