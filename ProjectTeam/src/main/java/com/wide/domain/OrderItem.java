package com.wide.domain;

public class OrderItem {
	private double price;
	private int quantity;
	private String name;
	private int productIdFk;
	private Order order;

	public OrderItem(String name, double price, int quantity, int productIdFk) {
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.productIdFk = productIdFk;
	}
	public int getProductIdFk() {
		return productIdFk;
	}
	public OrderItem() {
		
	}
	
	public String getName() {
		return name;
	}

	public double totalPrice() {
		return quantity * price;
	}
	public double getPrice() {
		return price;
	}
	public int getQuantity() {
		return quantity;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	public Order getOrder() {
		return order;
	}
	public String info() {
		return ("Price : "+ this.price+", Qty : "+this.quantity+"\n\tTotal Price : "+this.totalPrice());
	}
}
