package com.wide.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Out;

import com.wide.domain.Order;
import com.wide.domain.OrderItem;
import com.wide.domain.Product;
import com.wide.repository.OrderRepoFactory;
import com.wide.repository.OrderRepository;
import com.wide.repository.RepositoryException;

public class WebServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		OrderRepository orderRepo = OrderRepoFactory.createRepository();
		String action = req.getParameter("action");
		if ("listProduct".equals(action)) {
			try {
				req.setAttribute("list_product_data", orderRepo.productList());
				req.getRequestDispatcher("/listProduct.jsp").forward(req, resp);
			} catch (RepositoryException e) {
				System.out.println(e.getMessage());
			}
		} else if ("chooseProduct".equals(action)) {
			try {
				req.setAttribute("product_on_list", orderRepo.productList());
				req.getRequestDispatcher("/FormAddOrder.jsp").forward(req, resp);
			} catch (RepositoryException e) {
				System.out.println(e.getMessage());
			}
			
		} 
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		OrderRepository orderRepo = OrderRepoFactory.createRepository();
		String action = req.getParameter("action");
		if ("addProduct".equals(action)) {
			String code = req.getParameter("product_code");
			String name = req.getParameter("product_name");
			String type = req.getParameter("product_type");
			double price = Double.parseDouble(req.getParameter("product_price"));
			Product product = new Product(code,name,type,price);
			try {
				orderRepo.addProduct(product);
				req.setAttribute("add_product_data", product);
				resp.sendRedirect("orderTeamProject?action=listProduct");
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if ("deleteProduct".equals(action)) {
			int idProduct = Integer.parseInt(req.getParameter("product_id"));
			try {
				orderRepo.deleteProduct(idProduct);
				resp.sendRedirect("orderTeamProject?action=listProduct");
			} catch (RepositoryException e) {
				System.out.println(e.getMessage());
			}
		} else if("updateProduct".equals(action)) {
			int idProduct = Integer.parseInt(req.getParameter("id_product"));
			String code = req.getParameter("product_code");
			String name = req.getParameter("product_name");
			String type = req.getParameter("product_type");
			double price = Double.parseDouble(req.getParameter("product_price"));
			try {
				Product product = new Product(code,name,type,price);
				orderRepo.updateProduct(idProduct, product);
				resp.sendRedirect("orderTeamProject?action=listProduct");
			} catch (RepositoryException e) {
				System.out.println(e.getMessage());
			}
		} else if("getListProduct".equals(action)) {
			PrintWriter out = resp.getWriter();
			String data = req.getParameter("new_order");
			String[] dataSplit = data.split("-");
			int productId = Integer.parseInt(dataSplit[0].trim());
			String code = dataSplit[1].trim();
			String name = dataSplit[2].trim();
			String type = dataSplit[3].trim();
			double price = Double.parseDouble(dataSplit[4].trim());
			int quantity = Integer.parseInt(req.getParameter("product_quantity"));
			double totalPrice = price*quantity;
			ProductDto productDto = new ProductDto(productId, name, price, quantity, totalPrice);
			if(req.getSession().getAttribute("product_list") != null) {
				List<ProductDto> productsDtoSession = (List<ProductDto>) req.getSession().getAttribute("product_list");
				double totalOrderSession = 0;
				int newQuantity = 0;
				for(ProductDto pDto: productsDtoSession) {
					if(pDto.getProductDtoId() == productDto.getProductDtoId()) {
						newQuantity = pDto.getQuantity() + productDto.getQuantity();
						double newTotalPrice = newQuantity * pDto.getPrice();
						pDto.setQuantity(newQuantity);
						pDto.setTotalPrice(newTotalPrice);
					}
					totalOrderSession += pDto.getTotalPrice();
				}
				if(newQuantity == 0) {
					productsDtoSession.add(productDto);
					totalOrderSession = 0;
					for(ProductDto pDto: productsDtoSession) {
						totalOrderSession += pDto.getTotalPrice();
					}
				}
				req.getSession().setAttribute("total_order", totalOrderSession);
				req.getRequestDispatcher("/FormOrder.jsp").forward(req, resp);
			} else {
				List<ProductDto> productsDto = new ArrayList<ProductDto>();
				productsDto.add(productDto);
				req.getSession().setAttribute("product_list", productsDto);
				productDto.setTotalOrder(totalPrice);
				req.getSession().setAttribute("total_order", productDto.getTotalOrder());
				req.getRequestDispatcher("/FormOrder.jsp").forward(req, resp);
			}
				
		}
		else if("saveOrder".equals(action)) {
			List<ProductDto> productsDto = (List<ProductDto>) req.getSession().getAttribute("product_list");
			Order order = new Order();
			for(ProductDto pDto: productsDto) {
				order.addItem(new OrderItem(pDto.getName(), pDto.getPrice(), pDto.getQuantity(), pDto.getProductDtoId()));
			}
			try {
				orderRepo.save(order);
				req.getSession().invalidate();
				resp.sendRedirect("Save.jsp");
			} catch (RepositoryException e) {
				System.out.println(e.getMessage());
			}
//			PrintWriter out = resp.getWriter();
//			String[] dataName = req.getParameterValues("product_name");
//			String[] dataPrice = req.getParameterValues("product_price");
//			String[] dataQuantity = req.getParameterValues("product_quantity");
//			String[] dataTotalPrice = req.getParameterValues("product_total_price");
//			double dataTotalOrder = Double.parseDouble(req.getParameter("product_total_order"));
//			
//			ProductDto productDto = new ProductDto();
//			productDto.setTotalOrder(dataTotalOrder);
//			out.println(dataName.length);
//			out.println(dataPrice.length);
//			out.println(dataQuantity.length);
//			out.println(dataTotalPrice.length);			
//			out.println(productDto.getTotalOrder());	
		}
	}
}
