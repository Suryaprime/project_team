<%@page import="java.util.List"%>
<%@page import="com.wide.domain.Product"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%List<Product> products = (List<Product>)request.getAttribute("product_on_list"); %>
<form action="orderTeamProject?action=getListProduct" id="formGetListProduct" method="post" >
<h1 align="center">Product Item</h1>
<hr/>
Product
<br/>
<select name="new_order" form="formGetListProduct">
	<option>CHOOSE PRODUCT</option>
	<%int no = 1; %>
	<%for (int i=0; i < products.size(); i++) { %>
		<option itemref="${products}" ><%out.println(products.get(i).getProductId() + " - " +products.get(i).getCode()+" - "+ products.get(i).getName() + " - "+products.get(i).getType()+" - " + products.get(i).getPrice()); %></option>
	<%no++; %>
	<%}%>
</select>
<br/>
<br/>
Quantity <br/>
<input type="number" name="product_quantity"/>
<br/>

<input type="submit" value="ADD"/>
</form>
</body>
</html>