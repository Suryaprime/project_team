<%@page import="com.wide.domain.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>List Product</title>
</head>
<body>
<h1>List Product</h1>
<br/>
<h3><a href="http://localhost:8080/ProjectTeam/formAddProduct.jsp">Add Product</a>	|	<a href="http://localhost:8080/ProjectTeam/">Home</a></h3>
<h3></h3>
<h3></h3>
<br/>
<table border=1 class="styled-table">
<tr>
	<th>Code</th>
	<th>Name</th>
	<th>Type</th>
	<th>Price</th>
	<th>Edit</th>
</tr>
<c:forEach items="${list_product_data}" var="p">
	<tr>
		<td><c:out value="${p.getCode()}"></c:out></td>
		<td><c:out value="${p.getName()}"></c:out></td>
		<td><c:out value="${p.getType()}"></c:out></td>
		<td><c:out value="${p.getPrice()}"></c:out></td>
		<td><form action="orderTeamProject?action=deleteProduct" method="post"><button>Delete</button><input
									type="hidden" name="product_id"
									value="${p.getProductId()}" /></form> <br/>
		<form id="update" method="get" action="formUpdateProduct.jsp"><input
									type="hidden" name="product_id"
									value="${p.getProductId()}" /><input type="submit" value="Update"/></form></td>
	</tr>
</c:forEach>
</table>
</body>
</html>